const http = require('http');

const server = http.createServer((req, res) => {
  console.log('requisição feita');
  res.setHeader('Content-type', 'text/html');
  res.write(`
    <html>
      <head>
        <meta charset='utf-8'>
        <title>Olá, mundo!</title>
      </head>
      <body>
        Olá, mundo!
      </body>
    </html>
  `);

  res.end();
});

server.listen(3000);
