const users = require('./users');

const requestHandler = (req, res) => {
  /**
   * Informando que o retorno será HTML
   */
  res.setHeader('Content-type', 'text/html');

  /**
   * Rota padrão (/)
   */
  if (req.url === '/') {
    res.write('<h1>Hello from my first Node Server!</h1>');
    res.write(`
      <form action='/create-user' method='POST'>
        <label>
          Username: <input type='text' name='user'>
        </label>
        <input type='submit' value='Cadastrar'>
      </form>
    `);

    /**
     * O return é importante pois ao fim da requisição
     * nada mais poder ser enviado (erro "Headers already sent...")
     */
    return res.end();
  }

  /**
   * Rota 'create-user'
   */
  if (req.url === '/create-user' && req.method === 'POST') {
    /**
     * Inicializamos um vetor
     * e monitoramos 2 eventos, onData e onEnd de req
     */
    const data = [];

    /**
     * Adicionando 'pedaços' do retorno
     */
    req.on('data', chunk => {
      data.push(chunk);
    });

    /**
     * No fim da requisição
     * concatenamos os pedaços
     * e convertemos pra String
     */
    req.on('end', () => {
      const fullData = Buffer.concat(data).toString();

      /**
       * Inserindo o usuário no vetor
       * de usuários
       */
      const user = fullData.split('=')[1];
      users.push(user);
    });

    /**
     * Ao fim, redirecionamos novamente para /users
     * e finalizamos o retorno
     */
    res.statusCode = 302;
    res.setHeader('Location', '/users');
    return res.end();
  }

  if (req.url === '/users') {
    res.write('<ul>');

    users.forEach(user => {
      res.write(`<li>${user}</li>`);
    });
    res.write('</ul>');

    return res.end();
  }

  res.write('This route is not available');
  return res.end();
};

module.exports = requestHandler;
